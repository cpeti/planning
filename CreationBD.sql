-- @ SCRIPTS/CreationBD.sql;
-- @ SCRIPTS/instanceANIME;

DROP TABLE IF EXISTS EPISODE;
DROP TABLE IF EXISTS SAISON;
DROP TABLE IF EXISTS ANIME;


create table `ANIME` (
  `IdA` int(5) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(30),
  `Realisateur` varchar(20) DEFAULT NULL,
  `Genre` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`IdA`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

create table `SAISON` (
  `NumS` int(3) NOT NULL,
  `IdA` int(5) NOT NULL,
  `NomS` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`NumS`,`IdA`),
  FOREIGN KEY (IdA) REFERENCES ANIME(`IdA`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

create table `EPISODE` (
  `NumE` int(3) NOT NULL AUTO_INCREMENT,
  `NumS` int(3) NOT NULL,
  `IdA` int(5) NOT NULL,
  `NomE` varchar(30),
  `DateDiff` int(8),
  `Opening` varchar(30),
  `Ending` varchar(30),
  PRIMARY KEY (`NumE`,`NumS`,`IdA`),
  FOREIGN KEY (`NumS`) REFERENCES SAISON(`NumS`),
  FOREIGN KEY (`IdA`) REFERENCES ANIME(`IdA`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
