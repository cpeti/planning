<?php
if (isset($_POST['episode']) && isset($_POST['num']) && isset($_POST['season']) && isset($_POST['anime']) && isset($_POST['date']) && isset($_POST['op']) && isset($_POST['ed'])) {
	require "./bdd.php";
	$bdd = new AnimePDO();

	$saison = 1;
	$titre = filter_input(INPUT_POST, "episode", FILTER_SANITIZE_STRING);
	$saison = filter_input(INPUT_POST, "season", FILTER_VALIDATE_INT);
	$num = filter_input(INPUT_POST, "num", FILTER_VALIDATE_INT);
	$anime = filter_input(INPUT_POST, "anime", FILTER_VALIDATE_INT);
	$date = filter_input(INPUT_POST, "date", FILTER_VALIDATE_STRING);
	$op = filter_input(INPUT_POST, "op", FILTER_VALIDATE_STRING);
	$ed = filter_input(INPUT_POST, "ed", FILTER_VALIDATE_STRING);

	$bdd->add_episode($saison, $titre, $num, $date, $op, $ed);
}

header('Location: ../VUE/index.php');
