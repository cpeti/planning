<?php
if (isset($_POST['id'],$_POST['titre']) && isset($_POST['realisateur']) && isset($_POST['genre'])) {
	require "./bdd.php";
	$bdd = new AnimePDO();

  $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);
	$titre = filter_input(INPUT_POST, "titre", FILTER_SANITIZE_STRING);
	$realisateur = filter_input(INPUT_POST, "realisateur", FILTER_SANITIZE_STRING);
	$genre = filter_input(INPUT_POST, "genre", FILTER_SANITIZE_STRING);

	$bdd->modif_anime($titre, $realisateur, $genre,$id);
}

header('Location: ../VUE/index.php');
