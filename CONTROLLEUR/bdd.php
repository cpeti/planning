<?php
class AnimePDO extends PDO
{
	public function __construct()
	{
		parent::__construct('mysql:host=servinfo-mariadb;dbname=DBfouquet;charset=utf8', "fouquet", "fouquet");
		$this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// prepared statements
		// insertions
		$this->add_anime_statmt = $this->prepare('insert into ANIME (Titre, Realisateur, Genre) values (?, ?, ?)');

		//modif
		$this->modif_anime_statmt = $this->prepare('UPDATE `ANIME` SET `Titre`=?, `Realisateur`=?, `Genre`=? WHERE `idA`=?;');

		// selections
		$this->get_all_anime_statmt = $this->prepare('select * from ANIME');
		$this->get_anime_id_statmt = $this->prepare('select * from ANIME where idA = ?');
		$this->get_saison_id_statmt = $this->prepare('select * from SAISON where numS = ? and idA = ?');
		$this->get_episode_id_statmt = $this->prepare('select * from EPISODE where numE = ? and numS = ? and idA = ?');

		$this->get_saisons_anime_statmt = $this->prepare('select * from SAISON where idA = ?');
		$this->get_episodes_saison_statmt = $this->prepare('select * from EPISODE where numS = ? and idA = ?');

		$this->get_anime_titre_statmt = $this->prepare('select * from ANIME where Titre = ?');
	}

	public function add_anime($titre, $realisateur, $genre)
	{
		$this->add_anime_statmt->execute(array($titre, $realisateur, $genre));
		return true;
	}

	public function add_saison($idAn, $num, $titre)
	{
		$statmt = $this->prepare('insert into SAISON (numS, idA, NomS) values (?, ?, ?)');
		$statmt->execute(array($num, $idAn, $titre));
		return true;
	}

	public function add_episode($numS, $titre, $num, $date, $op, $ed)
	{
		$statmt = $this->prepare('insert into EPISODE (idS, idA, NomE, DateDiff, Opening, Ending) values (?, ?, ?, ?, ?, ?)');
		$statmt->execute(array($numS, $titre, $num, $date, $op, $ed));
		return true;
	}

	public function get_all_anime() {
		$this->get_all_anime_statmt->execute();
		$anime = $this->get_all_anime_statmt->fetchAll();
		$res = array();
		foreach ($anime as $serie) {
			$id = $serie['IdA'];
			$res[$id] = array('titre' => $serie['Titre'],
				'realisateur' => $serie['Realisateur'],
				'genre' => $serie['Genre'],
				'saisons' => array()
			);

			// Saisons
			$this->get_saisons_anime_statmt->execute(array($id));
			$saisons = $this->get_saisons_anime_statmt->fetchAll();
			foreach ($saisons as $saison) {
				$res[$id]['saisons'][$saison['NumS']] = array(
					'nom' => $saison['NomS'],
					'episodes' => array()
				);

				// Épisodes
				$this->get_episodes_saison_statmt->execute(array($saison['NumS'],$id));
				$episodes = $this->get_episodes_saison_statmt->fetchAll();
				foreach ($episodes as $episode) {
					$res[$id]['saisons'][$saison['NumS']]['episodes'][$episode['NumE']] = array(
						'nom' => $episode['NomE'],
						'date' => $episode['DateDiff'],
						'opening' => $episode['Opening'],
						'ending' => $episode['Ending']
					);
				}
			}
		}
		return $res;
	}

	public function get_anime($id)
	{
		$this->get_anime_id_statmt->execute(array($id));
		$anime = $this->get_anime_id_statmt->fetchAll();
		$res = array('id' => $anime[0]['IdA'],
				'titre' => $anime[0]['Titre'],
				'realisateur' => $anime[0]['Realisateur'],
				'genre' => $anime[0]['Genre'],
				'saisons' => array()
		);

		// Saisons
		$this->get_saisons_anime_statmt->execute(array($id));
		$saisons = $this->get_saisons_anime_statmt->fetchAll();
		foreach ($saisons as $saison) {
			$res['saisons'][$saison['NumS']] = array(
				'nom' => $saison['NomS'],
				'episodes' => array()
			);

			// Épisodes
			$this->get_episodes_saison_statmt->execute(array($saison['NumS'],$res['id']));
			$episodes = $this->get_episodes_saison_statmt->fetchAll();
			foreach ($episodes as $episode) {
				$res['saisons'][$saison['NumS']]['episodes'][$episode['NumE']] = array(
					'nom' => $episode['NomE'],
					'date' => $episode['DateDiff'],
					'opening' => $episode['Opening'],
					'ending' => $episode['Ending']
				);
			}
		}

		return $res;
	}

	public function get_saison($numS, $idA)
	{
		$this->get_saison_id_statmt->execute(array($numS, $idA));
		$saison = $this->get_saison_id_statmt->fetchAll()[0];
		$res = array('num' => $numS,
			'anime' => $saison['IdA'],
			'nom' => $saison['NomS'],
			'episodes' => array()
		);

		// Épisodes
		$this->get_episodes_saison_statmt->execute(array($id));
		$episodes = $this->get_episodes_saison_statmt->fetchAll();
		foreach ($episodes as $episode) {
			$res['episodes'][$episode['NumE']] = array(
				'nom' => $episode['NomE'],
				'date' => $episode['DateDiff'],
				'opening' => $episode['Opening'],
				'ending' => $episode['Ending']
			);
		}

		return $res;
	}

	public function get_episode($numE, $numS, $idA)
	{
		$this->get_episode_id_statmt->execute(array($numE, $numS, $idA));
		$episode = $this->get_episode_id_statmt->fetchAll()[0];
		$res = array('num' => $id,
			'saison' => $episode['NumS'],
			'nom' => $episode['NomE'],
			'date' => $episode['DateDiff'],
			'opening' => $episode['Opening'],
			'ending' => $episode['Ending']
		);
		return $res;
	}


	public function modif_anime($titre, $realisateur, $genre, $id)
	{
		$statement = $this->prepare('UPDATE `ANIME` SET `Titre`=?, `Realisateur`=?, `Genre`=? WHERE `idA`=?;');
		$statement->execute(array($titre,$realisateur,$genre,$id));
	}
	public function search_anime($anime, $realisateur, $genre) {
		$sql = "select * from ANIME where Titre like concat('%', ?, '%') and Realisateur like concat('%', ?, '%') and Genre like concat('%', ?, '%')";
		$stmt = $this->prepare($sql);

		$stmt->execute(array($anime, $realisateur, $genre));

		$res = array();
		$anime = $stmt->fetchAll();
		foreach ($anime as $serie) {
			$res[$serie['IdA']] = array(
				'IdA' =>  $serie['IdA'],
				'titre' => $serie['Titre'],
				'realisateur' => $serie['Realisateur'],
				'genre' => $serie['Genre']
			);
		}

		return $res;
	}
}
?>
