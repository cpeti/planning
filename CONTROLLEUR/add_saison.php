<?php
if (isset($_POST['season']) && isset($_POST['num']) && isset($_POST['anime'])) {
	require "./bdd.php";
	$bdd = new AnimePDO();

	$anime = filter_input(INPUT_POST, "anime", FILTER_VALIDATE_INT);
	$num = filter_input(INPUT_POST, "num", FILTER_VALIDATE_INT);
	$nom = filter_input(INPUT_POST, "season", FILTER_SANITIZE_STRING);

	$bdd->add_saison($anime, $num, $nom);
	header('Location: ../VUE/anime.php?id='.$anime);
}

header('Location: ../VUE/index.php');
