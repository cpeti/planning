<?php
if (isset($_POST['titre']) && isset($_POST['realisateur']) && isset($_POST['genre'])) {
	require "./bdd.php";
	$bdd = new AnimePDO();

	$titre = filter_input(INPUT_POST, "titre", FILTER_SANITIZE_STRING);
	$realisateur = filter_input(INPUT_POST, "realisateur", FILTER_SANITIZE_STRING);
	$genre = filter_input(INPUT_POST, "genre", FILTER_SANITIZE_STRING);

	$bdd->add_anime($titre, $realisateur, $genre);
}

header('Location: ../VUE/add-anime.html');
