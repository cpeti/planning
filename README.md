# planning

19 octobre date de rendu

membres du groupes : Antoine BEAUVAIS, Amélie DAUVOIS, Étienne FOUQUET

On veut réaliser une application de gestion d’une collection d'animes en PHP. Cette application ne demande pas
d’authentification mais doit permettre de :

—  Consulter la liste d'animes disponibles selon différents critères

—  Gerer les titres, realisateurs, annees de realisation et genre (policier, comedie, drame etc.)

—  Ajouter un anime

—  Supprimer un anime

—  Gérer les épisode et les saisons

On considerera qu’un anime a un seul realisateur et un seul genre. Nourrissez votre base avec une liste de anime recuperes sur le Web. Inutile de prendre une base trop importante. Options : gérer la modification des animes et les affiches de d'anime, qu’on récuperera  ́eventuellement aussi sur le Web. Examinez de près cette demande et donnez en une analyse et une implémentation SQL pour commencer.

mysql -h servinfo-mariadb -u fouquet -p

http://localhost/~fouquet/phpMyAdmin/index.php
