<?php
require "../CONTROLLEUR/bdd.php";
$bdd = new AnimePDO();

$anime = $bdd->get_all_anime();
//var_dump($anime);
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Ajout episode- Bdanime</title>
    <link rel="stylesheet" type="text/css" href="./add-anime.css" media="screen" />
  </head>

  <body>
	<nav>
		<img id="banner" alt="banniere" src="../img/banniere4.png" title="banniere">
		<ul>
		  <li><a href="./index.php">Accueil</a></li>
		  <li><a href="./add-anime.html">Ajouter anime</a></li>
		</ul>
	</nav>

	<article>
	  <h1>Ajout d'une épisode</h1>

      <form name="addseason" action="../CONTROLLEUR/add_episode.php" method="POST">
	    <input type="hidden" name="anime" value=<?php echo $_GET['id']?>>
      <input type="hidden" name="saison" value=<?php echo $_GET['saison']?>>
	    <p>
        Numéro : <input type="int" name="num" placeholder="1">
        </p>
	    <p>
        Titre : <input type="text" name="titre" placeholder="1">
      </p>
      <p>
        Opening : <input type="text" name="op" placeholder="1">
      </p>
      <p>
        Ending : <input type="text" name="ed" placeholder="1">
      </p>
      <p>
        dateDif : <input type="text" name="date" placeholder="1">
      </p>
        <input type="submit" value="Valider"/>
      </form>
      <br/>
	</article>

  </body>
</html>
