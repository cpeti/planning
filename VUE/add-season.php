<?php
require "../CONTROLLEUR/bdd.php";
$bdd = new AnimePDO();

$anime = $bdd->get_all_anime();
//var_dump($anime);
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Ajout saison- Bdanime</title>
    <link rel="stylesheet" type="text/css" href="./add-anime.css" media="screen" />
  </head>

  <body>
	<nav>
		<img id="banner" alt="banniere" src="../img/banniere4.png" title="banniere">
		<ul>
		  <li><a href="./index.php">Accueil</a></li>
		  <li><a href="./add-anime.html">Ajouter anime</a></li>
		</ul>
	</nav>

	<article>
	  <h1>Ajout d'une saison</h1>

      <form name="addseason" action="../CONTROLLEUR/add_saison.php" method="POST">
	    <input type="hidden" name="anime" value=<?php echo $_GET['id']?>>
	    <p>
        Numéro : <input type="int" name="num" placeholder="1">
        </p>
	    <p>
        Titre : <input type="text" name="season" placeholder="1">
        </p>
        <input type="submit" value="Valider"/>
      </form>
      <br/>
	</article>

  </body>
</html>
