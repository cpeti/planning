<?php
if (isset($_GET['id'])) {
	require "../CONTROLLEUR/bdd.php";
	$bdd = new AnimePDO();
	$res = $bdd->get_anime($_GET['id']);
	$id = $res['id'];
    $titre = $res['titre'];
    $realisateur = $res['realisateur'];
    $genre = $res['genre'];
  }
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Ajout - Base de donn&eacute;es d'animes</title>
    <link rel="stylesheet" type="text/css" href="./modif-anime.css" media="screen" />
  </head>

  <body>
	<header>
	  <nav>
        <img id="banner" alt="banniere" src="../img/banniere4.png" title="banniere">
		<ul>
		  <li><a href="./index.php">Accueil</a></li>
		  <li><a href="./add-anime.html">Ajouter anime</a></li>
		</ul>
	  </nav>
	</header>

    <article>
      <h2>Modifier anime</h2>

      <!-- Form data and parameters are placeholders. -->
      <form name="add" action="../CONTROLLEUR/modif_anime.php" method="POST">
	      <p>
            Titre : <br/>
			<input type="text" name="titre" value="<?=$titre?>">
          </p>
          <p>
            R&eacute;alisateur : <br/>
			<input type="text" name="realisateur" value="<?=$realisateur?>">
          </p>
          <p>
            Genre : <br/>
			<input type="text" name="genre" value="<?=$genre?>">
          </p>
        <input type="hidden" name="id" value=<?php echo $_GET['id']?>>
        <input type="submit" value="Valider"/>
      </form>
      <form enctype="multipart/form-data" action="../CONTROLLEUR/upload_image.php" method="post">
        <fieldset>
        <legend>Formulaire</legend>
          <p>
            <label for="fichier_a_uploader" title="Recherchez le fichier à uploader !">Envoyer le fichier :</label>
            <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
            <input type="hidden" name="IdA" value=<?php echo $_GET['id']?> />
            <input name="fichier" type="file" id="fichier_a_uploader" />
            <input type="submit" name="submit" value="Uploader" />
          </p>
      </fieldset>
    </form>
    </article>
  </body>
</html>
