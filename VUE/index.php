<!DOCTYPE html>
<?php
require "../CONTROLLEUR/bdd.php";
$bdd = new AnimePDO();

$anime = $bdd->get_all_anime();
//var_dump($anime);
?>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Base de donn&eacute;es d'animes</title>
    <link rel="stylesheet" type="text/css" href="./index.css" media="screen" />
  </head>

  <body>
	<header>
	  <img id="banner" alt="Banni&egrave;re" src="../img/banniere4.png" title="banniere">
	  <nav>
		<ul>
		  <li><a href="./index.php">Accueil</a></li>
		  <li><a href="./add-anime.html">Ajouter anime</a></li>
		</ul>
	  </nav>
	</header>
	<article>
		<h2>Recherche</h2>
  <form name="search" method="GET" action="search.php">
  <p>
	Anime <br/>
	<input list="anime" name="anime" placeholder="Revolutionary Girl Utena"/>
	<datalist id="anime">
		<?php foreach ($anime as $serie) { ?>
		<option><?=$serie['titre']?></option>
		<?php } ?>
	</datalist>
  </p>
  <p>
	Réalisateur <br />
	<input list="realisateur" name="realisateur" placeholder="Kunihiko Ikuhara"/>
	<datalist id="realisateur">
		<?php foreach ($anime as $serie) { ?>
		<option><?=$serie['realisateur']?></option>
		<?php } ?>
	</datalist>
  </p>
  <p>
	Genre <br/>
	<input list="genre" name="genre" placeholder="Drame"/>
	<datalist id="genre">
		<?php foreach ($anime as $serie) { ?>
		<option><?=$serie['genre']?></option>
		<?php } ?>
	</datalist>
  </p>

  <br/>
  <input type="submit" value="Rechercher">
  <br/>

  </form>
      <p>Prototype de base de donn&eacute;es d'animes en tant que projet Serveur Web.</p>
	</article>
  </body>
</html>
