
<?php
if (isset($_GET['anime']) && isset($_GET['realisateur']) && isset($_GET['genre'])) {
	require "../CONTROLLEUR/bdd.php";
	$bdd = new AnimePDO();
	$anime = filter_input(INPUT_GET, "anime", FILTER_SANITIZE_STRING);
	$realisateur = filter_input(INPUT_GET, "realisateur", FILTER_SANITIZE_STRING);
	$genre = filter_input(INPUT_GET, "genre", FILTER_SANITIZE_STRING);
	$listeAnime=$bdd->search_anime($anime, $realisateur, $genre);
	}
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Recherche - Bdanime</title>
    <link rel="stylesheet" type="text/css" href="./search.css" media="screen" />
  </head>

  <body>
    <header>
	  <nav>
    <img id="banner" alt="banniere" src="../img/banniere4.png" title="banniere">
		<ul>
		  <li><a href="./index.php">Accueil</a></li>
		  <li><a href="./add-anime.html">Ajouter anime</a></li>
		</ul>
	  </nav>
	  <h1>R&eacute;sultats de la recherche</h1>
	</header>

    <article>
		<?php
		foreach ($listeAnime as $serie) {
			?>
			<form name="recherche" method="GET" action="anime.php">
				<input name="anime" type="hidden" value="<?php echo $serie['IdA'] ?>"/>
				<ul>
					<li><input class="title" type="submit" value="<?php echo $serie['titre'] ?>"></li>
					<li><input class="cover" type="image" src="../img/<?php echo $serie['IdA'] ?>.jpg" alt="<?php echo $serie['titre'] ?>"></li>
					<li><?php echo $serie['realisateur'] ?></li>
					<li><?php echo $serie['genre'] ?></li>
				</ul>
		    </form>
			<?php
		}
		?>
    </article>
  </body>
</html>
