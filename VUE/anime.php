<?php
if (isset($_GET['anime'])) {
	require "../CONTROLLEUR/bdd.php";
	$id = filter_input(INPUT_GET, "anime", FILTER_VALIDATE_INT);
	$bdd = new AnimePDO();
	$res = $bdd->get_anime($id);
    $titre = $res['titre'];
    $realisateur = $res['realisateur'];
    $genre = $res['genre'];
	$saisons = $res['saisons'];
  }
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title><?php echo $titre ?> - Bdanime</title>
    <link rel="stylesheet" type="text/css" href="./anime.css" media="screen" />
  </head>

  <body>
    <header>
	  <nav>
    <img id="banner" alt="banniere" src="../img/banniere4.png" title="banniere">
		<ul>
		  <li><a href="./index.php">Accueil</a></li>
		  <li><a href="./add-anime.html">Ajouter anime</a></li>
		</ul>
	  </nav>
	</header>

	<article>
	  <ul>
	      <li id="title"><?php echo $titre ?></li>
	      <li id="cover"><img src="../img/<?php echo $id ?>.jpg" alt="<?php echo $titre ?>"></li>
	      <li>R&eacute;alisateur         <?php echo $realisateur ?></li>
	      <li>Genre                      <?php echo $genre ?></li>
	      <form name="edit" action="./modif-anime.php" method="GET"/>
	        <input type="hidden" id="id" name="id" value="<?php echo $id ?>"/>
	        <li><input class="edit" type="submit" value="Modifier"/></li>
	      </form>
	      <form name="addseason" action="./add-season.php" method="GET">
			<input type="hidden" id="id" name="id" value="<?php echo $id ?>"/>
	        <li><input class="edit" type="submit" value="Ajouter une saison, un épisode..."/></li>
	      </form>
	  </ul>
	</article>
	<?php
		foreach ($res['saisons'] as $idS => $saison) {
	?>
			<article>
				<p><?php echo $saison['nom'] ?></p>
				<form name="addseason" action="./add-episode.php" method="GET" >
					<input type="hidden" id="id" name="id" value="<?php echo $id ?>"/>
					<input type="hidden" id="saison" name="saison" value="<?php echo $idS ?>"/>
					<li><input class="edit" type="submit" value="+"/></li>
				</form>
				<ul>
					<li>numéro:</li>
					<li>date diffussion:</li>
					<li>openning:</li>
					<li>ending:</li>
				</ul>
					<?php
						foreach ($saison['episodes'] as $id => $episode) {
					?>
							<ul>
								<li><?php echo $id ?></li>
								<li><?php echo $episode['date'] ?></li>
								<li><?php echo $episode['opening'] ?></li>
								<li><?php echo $episode['ending'] ?></li>
							</ul>
					<?php
						}
						?>
			</article>
	<?php
		}
		?>
  </body>
</html>
